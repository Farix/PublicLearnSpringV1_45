package main;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.*;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = main.Calculator.class)
public class CalculatorTest {
    @Autowired
    private Calculator calculator;
    @Test
    public void add() throws Exception {
        double test = calculator.add(1, 1 );
        assertEquals(test, 2.0);
        System.out.println("test:CalculatorTest.add finished");

    }

    @Test
    public void minus() throws Exception {
    }

}