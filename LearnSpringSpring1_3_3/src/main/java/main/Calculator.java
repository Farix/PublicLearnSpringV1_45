package main;

import org.springframework.stereotype.Component;

@Component
public class Calculator {

    public double add(double a, double b) {
        return a + b;
    }

    public double minus(double a, double b){
        return a - b;
    }

    public Calculator() {
    }
}
